package libbot

import (
	"github.com/bwmarrin/discordgo"
	"strings"
	"github.com/kballard/go-shellquote"
)

// Prints all commands
// Default value for `l.Commands["help"]`
var HelpCommand = &CommandHandler{
	Handler: func(s string, args []string, ctx *Context) {
		if len(args) == 1 {
			h, ok := ctx.LibBot.Commands[args[0]]
			if !ok {
				ctx.SendMessage("Unknown command.")
				return
			}
			ctx.SendMessage(ctx.Config.CommandPrefix + args[0] + " " + h.Usage + ": " + "\n" + h.HelpLong)
			return
		}
		s = "Commands:"

		for cmd, h := range ctx.LibBot.Commands {
			s += "\n- " + ctx.Config.CommandPrefix + cmd + " " + h.Usage + ": " + h.HelpShort
		}
		ctx.SendMessage(s)
	},
	Usage: "[cmd]",
	HelpShort: "Prints all commands and their short usage text",
	HelpLong:  `Prints all commands and their short usage text

Eg:
Commands:
- !help [cmd]: Prints all commands and their short usage text
- !author : Prints the author of me
- !check-permission <perm>: Checks your permission`,
}

// Prints the author
// Default value for `l.Commands["author"]`
var AuthorCommand = &CommandHandler{
	Handler: func(s string, args []string, ctx *Context) {
		u, err := ctx.LibBot.Session.User(ctx.LibBot.Options.CreatorId)
		if err != nil {
			ctx.SendMessage("I'm a bot by:\n<Unknown>")
			return
		}

		ctx.SendMessage("I'm a bot by:\n@" + u.Username + "#" + u.Discriminator)
	},
	Usage: "",
	HelpShort: "Prints the author of me",
	HelpLong:  `Prints the author of me

Eg:
I'm a bot by:
@Ma_124#4953`,
}

// Checks permission
// Prints "Permission granted." or "Permission denied."
// Default value for `l.Commands["check-permission"]`
var CheckPermissionCommand = &CommandHandler{
	Handler: func(s string, args []string, ctx *Context) {
		if len(args) == 1 {
			ctx.CheckPermission("Permission not specified. Expected: " + ctx.Config.CommandPrefix + "check-permission <perm>")
			return
		}

		if ctx.CheckPermission(args[0]) {
			ctx.SendMessage("Permission granted.")
		} else {
			ctx.SendMessage("Permission denied.")
		}
	},
	Usage: "<perm>",
	HelpShort: "Checks your permission",
	HelpLong:  `Checks your permission.

Eg:
Permission granted.

Permission denied.`,
}

type CommandHandler struct {
	Handler   func(s string, args []string, ctx *Context)
	Usage string
	HelpShort string
	HelpLong  string
}

func (l *LibBot) initCmd() {
	l.Commands["help"] = HelpCommand
	l.Commands["author"] = AuthorCommand
	l.Commands["check-permission"] = CheckPermissionCommand

	l.Session.AddHandler(func(_ *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == l.Session.State.User.ID {
			return
		}

		ctx, err := l.NewContextFromMessage(m.Message)
		if err != nil {
			l.logErr(err)
		}

		if ctx.Channel.Type == discordgo.ChannelTypeDM {
			dmMessageCreateHandler(m, ctx)
			return
		}

		if strings.HasPrefix(m.Content, ctx.Config.CommandPrefix) {
			for cmd, h := range l.Commands {
				if strings.HasPrefix(m.Content[len(ctx.Config.CommandPrefix):], cmd) {
					var s string
					if m.Content == ctx.Config.CommandPrefix + cmd {
						s = m.Content[len(ctx.Config.CommandPrefix) + len(cmd):]
					} else {
						s = m.Content[len(ctx.Config.CommandPrefix) + len(cmd) + 1:]
					}

					args, err := shellquote.Split(s)
					if err != nil {
						args = strings.Split(s, " ")
					}
					h.Handler(s, args, ctx)
					return
				}
			}

			ctx.SendMessage("Command not found")
		}
	})
}
