`libdcbot`
==========
[![GoDoc](https://camo.githubusercontent.com/778b4c7295d97761d586024fcbf757089d534fd8/68747470733a2f2f676f646f632e6f72672f6769746875622e636f6d2f6a726f696d617274696e2f676f6375693f7374617475732e737667)](https://godoc.org/gitlab.com/Ma_124/libdcbot)

A library for building Discord-Bots.

Table of Contents
-----------------

* [Examples](#examples)
    * [Basic](#basics)
    * [`CheckPermission`](#checkpermission)
    * [`SendUI`](#sendui)
* [Features](#features)
* [Tips](#tips)
    * [Enable Discord Developer Mode](#enable-discord-developer-mode)

Examples
--------

### Basics

```go
// test.go

package main

import "gitlab.com/Ma_124/libdcbot"

func main() {
  libbot.InitFromEnv(&libbot.InitOption{
    // If you don't know how to get your own ID see Tips/Enable Discord Developer Mode
    CreatorId: "239288005280399360",
  }, func(l *libbot.LibBot) {
    l.Commands["ping"] = &libbot.CommandHandler{
      Handler: func(s string, args []string, ctx *libbot.Context) {
        if len(args) == 1 {
          ctx.SendMessage(args[0] + "!")
        } else {
          ctx.SendMessage("Pong!")
        }
      },
      Usage:     "[msg]",
      HelpShort: "A short help text",
      HelpLong:  "A really long\nmultiline description",
    }
  })
}
```

```bash
$ LIBBOT_TOKEN="Y0UR.AP1.K3Y" go run test.go
Press Ctrl-C to exit.
```

Now you can invite your bot to a server and type `!ping "Hello World"` and it should respond with `Hello World!`

### `CheckPermission`

```go
// test2.go

package main

import "gitlab.com/Ma_124/libdcbot"

func main() {
  // You won't need to specify $LIBBOT_TOKEN this time in the shell because we hardcoded it. This is generally bad practice
  // because then you can't commit your bot to a public VCS like GitHub or GitLab
  // without exposing your KEY WHICH YOU SHOULD KEEP PRIVATE AT ALL TIMES.
  // If you still somehow publish your key by accident you can go to https://discordapp.com/developers/applications/<APP_ID>/bots to reset it
  l, err := libbot.InitBot("Y0UR.AP1.K3Y", &libbot.InitOption{ /* ... */ })
  if err != nil {
    panic(err)
  }

  l.Commands["init"] = &libbot.CommandHandler{
    Handler: func(s string, args []string, ctx *libbot.Context) {
      ctx.Config.Permissions["custom_perm"] = libbot.PermissionConfig{
        Roles: []string{"Custom Perm Role"},
        // or use: Users: []string{"YourName#1337"},
      }
    },
    /* ... */
  }

  l.Commands["test"] = &libbot.CommandHandler{
    Handler: func(s string, args []string, ctx *libbot.Context) {
      if !ctx.CheckPermission("custom_perm") {
        ctx.SendMessage("Permission denied.")
        return
      }

      ctx.CheckPermission("Permission granted.")
    },
    /* ... */
  }

  l.WaitClose()
}
```

Run it

```bash
$ go run test2.go
Press Ctrl-C to exit.
```

and create a role called `Custom Perm Role` and call `!init` and then `!test`. You should see `Permission denied.`. Now join the `Custom Perm Role` and call `!test` again. You should see `Permission granted`.

### `SendUI`

```go
// test3.go

package main

import (
  "math/rand"
  "github.com/bwmarrin/discordgo"
  "gitlab.com/Ma_124/libdcbot"
)

func main() {
  libbot.InitFromEnv(&libbot.InitOption{ /* ... */ }, func(l *libbot.LibBot) {
    l.Commands["test"] = &libbot.CommandHandler{
      Handler: func(s string, args []string, ctx *libbot.Context) {
        ctx.SendUI(&discordgo.MessageEmbed{
          Title: "Hello World",
          // Random color RED GREEN BLUE
          //           0x  FF   FF  FF
          Color: rand.Intn(0xFFFFFF),
        }, func(ctx *libbot.Context, ev *discordgo.MessageReaction) {
          msg, _ := ctx.LibBot.Session.ChannelMessage(ev.ChannelID, ev.MessageID)
          msg.Embeds[0].Description += "+ " + ev.Emoji.Name + " "

          ctx.LibBot.Session.ChannelMessageEditComplex(&discordgo.MessageEdit{
            ID:      ev.MessageID,
            Channel: msg.ChannelID,
            Embed:   msg.Embeds[0],
          })
        }, func(ctx *libbot.Context, ev *discordgo.MessageReaction) {
          msg, _ := ctx.LibBot.Session.ChannelMessage(ev.ChannelID, ev.MessageID)
          msg.Embeds[0].Description += "- " + ev.Emoji.Name + " "

          ctx.LibBot.Session.ChannelMessageEditComplex(&discordgo.MessageEdit{
            ID:      ev.MessageID,
            Channel: msg.ChannelID,
            Embed:   msg.Embeds[0],
          })
        })
      },
    }
  })
}
```

Now send `!test` and add reactions and remove them again.
You should see that the that every time you add an emoji a `+ <emoji>` appears and every time you remove one `- <emoji>`

Features
--------

- [x] Lifecycle management
- [x] Commands
	- [x] Custom commands
	- [x] Arguments
	- [x] Default commands (!help, !author, !check-permission)
	- [x] Custom command prefixes
	- [ ] Parse `args` into structs
- [x] Permission management
	- [x] Groups
	- [x] Roles
	- [x] Custom users
- [ ] Forms
	- [x] Basic forms
	- [x] DM forms
	- [ ] Web forms
- [x] EmbedUI
	- [x] Send Embed
	- [x] Emoji reaction handlers
	- [ ] (maybe future improvements)
- [ ] Logging and error management
	- [x] Rudementary
	- [ ] Necessary functions are called
	- [ ] Good Discord messages
	- [ ] Good logging
	- [ ] Accessibilty for non internal caller
- [ ] Documentation
	- [x] README
	- [x] Go Doc
	- [ ] 100% go doc
	- [x] Features
	- [x] Basics
	- [x] `CheckPermission`
	- [x] `SendUI`
	- [ ] Forms
- [ ] Testing
    - [ ] Let a bot issue commands and monitor their responses
    - [ ] etc.

Tips
----

### Enable Discord Developer Mode

![Settings > Appearance > Developer Mode](https://gitlab.com/Ma_124/pages-static-shared/raw/master/discord_devmode.png)

Now you can right click on an user, a channel or a guild and copy it's ID
