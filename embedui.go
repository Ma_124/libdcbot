package libbot

import (
	"github.com/bwmarrin/discordgo"
)

var msgReactAddHands = make(map[string]func(ctx *Context, ev *discordgo.MessageReaction))
var msgReactRmHands = make(map[string]func(ctx *Context, ev *discordgo.MessageReaction))

func (l *LibBot) initEmbedUi() {
	l.Session.AddHandler(func(s *discordgo.Session, ev *discordgo.MessageReactionAdd) {
		ctx, err := l.NewContextFromMessageReaction(ev.MessageReaction)
		if err != nil {
			l.logErr(err)
		}

		h := msgReactAddHands[ev.ChannelID + "/" + ev.MessageID]
		if h != nil {
			h(ctx, ev.MessageReaction)
		}
	})

	l.Session.AddHandler(func(s *discordgo.Session, ev *discordgo.MessageReactionRemove) {
		ctx, err := l.NewContextFromMessageReaction(ev.MessageReaction)
		if err != nil {
			l.logErr(err)
		}

		h := msgReactRmHands[ev.ChannelID + "/" + ev.MessageID]
		if h != nil {
			h(ctx, ev.MessageReaction)
		}
	})
}

func (ctx *Context) SendUI(embed *discordgo.MessageEmbed, msgReactAddHand func(ctx *Context, ev *discordgo.MessageReaction), msgReactRmHand func(ctx *Context, ev *discordgo.MessageReaction)) error {
	msg, err := ctx.LibBot.Session.ChannelMessageSendComplex(ctx.ChannelId, &discordgo.MessageSend{
		Embed: embed,
	})

	if err != nil {
		return err
	}

	msgReactAddHands[msg.ChannelID + "/" + msg.ID] = msgReactAddHand
	msgReactRmHands[msg.ChannelID + "/" + msg.ID] = msgReactRmHand

	return nil
}
