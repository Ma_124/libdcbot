package libbot

import "fmt"

func (ctx *Context) handleErr(err error) {
	ctx.LibBot.logErr(err)

	if ctx != nil {
		ctx.LibBot.Session.ChannelMessageSend(ctx.ChannelId, "Error: ```" + err.Error() + "```")
	}
}

func (l *LibBot) logErr(err error) {
	fmt.Println(err)
}
