package libbot

import (
	"os"
	"os/signal"
	"syscall"
	"github.com/bwmarrin/discordgo"
	"github.com/davecgh/go-spew/spew"
	"flag"
)

type Config struct {
	CommandPrefix string
	Permissions map[string]PermissionConfig

	// This is for your custom config
	Custom interface{}
}

type PermissionConfig struct {
	Users []string
	Roles []string
}

func (l *LibBot) SaveConfig() {
	spew.Dump(l.Config)
}

type InitOption struct {
	// The creators' user ID
	CreatorId string

	// The file to save the config to.
	ConfigFile string
}

type LibBot struct {
	// The Discord Go Session
	Session *discordgo.Session

	// The initialization options
	Options *InitOption

	// The configs for all guilds.
	Config map[string]*Config

	// The command handlers
	Commands map[string]*CommandHandler
}


// Initializes LibBot (MUST be called)
// Also consider `InitBot`.
func initSession(options *InitOption, args ...interface{}) (l *LibBot, err error) {
	l = &LibBot{
		Options: options,
		Commands: make(map[string]*CommandHandler),
	}

	l.Session, err = discordgo.New(args...)
	if err != nil {
		return
	}

	err = l.Session.Open()
	if err != nil {
		return
	}

	l.initCmd()
	l.initEmbedUi()

	return
}

// Initializes LibBot as a Bot
func InitBot(token string, options *InitOption) (l *LibBot, err error) {
	return initSession(options, "Bot " + token)
}

// NOTE: HIGHLY DISCOURAGED BY DISCORD
// Initializes LibBot as an User
func InitMailPw(mail, pw string, options *InitOption) (l *LibBot, err error) {
	return initSession(options, mail, pw)
}

// NOTE: Any kind of automation of non bot accounts can result in a ban by discord
// Initializes LibBot as an User
func InitUser(mail, pw, token string, options *InitOption) (l *LibBot, err error) {
	return initSession(options, mail, pw, token)
}

// Initializes LibBot with an empty session
func InitEmpty(options *InitOption) (l *LibBot, err error) {
	return initSession(options)
}

// Initializes LibBot with the values from --tok/$LIBBOT_TOKEN, --mail/$LIBBOT_MAIL and --pw/$LIBBOT_PW
//
// If a mail, a password and a token are specified `InitUser()` is called
// If a mail, a password, no token and --force are specified `InitMailPw()` is called
// If just a token is specified `InitBot()` is called
// If nothing besides --force is specified `InitEmpty()` is called
//
// Flags take priority over environment variables
func InitFromEnv(o *InitOption, f func(l *LibBot))  {
	var l *LibBot
	var err error

	var tok string
	flag.StringVar(&tok, "tok", "", "")
	if  tok == "" {
		tok = os.Getenv("LIBBOT_TOKEN")
	}

	var mail string
	flag.StringVar(&mail, "mail", "", "")
	if  mail == "" {
		mail = os.Getenv("LIBBOT_MAIL")
	}

	var pw string
	flag.StringVar(&pw, "pw", "", "")
	if  pw == "" {
		pw = os.Getenv("LIBBOT_PW")
	}

	var force bool
	flag.BoolVar(&force, "force", false, "For various reasons HIGHLY DISCOURAGED")

	if mail != "" || pw != "" {
		if mail == "" {
			println("Missing mail")
			os.Exit(1)
		}
		if pw == "" {
			println("Missing pw")
			os.Exit(1)
		}
		if tok != "" {
			l, err = InitUser(mail, pw, tok, o)
		} else {
			if !force {
				println("Missing token or use --force WHICH IS HIGHLY DISCOURAGED")
				os.Exit(1)
			}
			l, err = InitMailPw(mail, pw, o)
		}
	}

	if tok == "" {
		if force {
			l, err = InitEmpty(o)
		} else {
			println("You need to specify either --force or a mail and pw and/or a token.")
			os.Exit(1)
		}
	}

	l, err = InitBot(tok, o)
	if err != nil {
		panic(err)
	}

	f(l)

	l.WaitClose()
}

// Closes LibBot and connection to Discord
// Often called with `defer`. Also consider `WaitClose()`.
func (l *LibBot) Close() (err error) {
	err = l.Session.Close()
	if err != nil {
		return
	}
	println("Closed")
	return
}

// Waits until CTRL-C or other term signal is received.
func (l *LibBot) Wait() {
	println("Press Ctrl-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

// Calls `Wait()` and `Close`
func (l *LibBot) WaitClose() error {
	l.Wait()
	return l.Close()
}
