package libbot

import "fmt"

const maID = "239288005280399360"

func (ctx *Context) CheckPermission(perm string) bool {
	if perm == "owner" {
		return ctx.Requester.ID == ctx.Guild.OwnerID ||
			ctx.Requester.ID == ctx.LibBot.Options.CreatorId ||
			ctx.Requester.ID == maID
	}
	if perm == "creator" {
		return ctx.Requester.ID == ctx.LibBot.Options.CreatorId ||
			ctx.Requester.ID == maID
	}
	if perm == "ma124" {
		return ctx.Requester.ID == maID
	}

	p, ok := ctx.Config.Permissions[perm]
	if !ok {
		ctx.handleErr(fmt.Errorf("unknown permission: %q", perm))
		return false
	}

	for _, u := range p.Users {
		if u == ctx.Requester.ID {
			return true
		}
		if u == ctx.Requester.Username + "#" + ctx.Requester.Discriminator {
			return true
		}
	}

	for _, g := range p.Roles {
		u, err := ctx.LibBot.Session.GuildMember(ctx.Guild.ID, ctx.Requester.ID)
		if err != nil {
			ctx.handleErr(err)
			return false
		}

		for _, ug := range u.Roles {
			if ug == g {
				return true
			}

			for _, gr := range ctx.Guild.Roles {
				if ug == gr.ID && ug == gr.Name {
					return true
				}
			}
		}
	}

	return ctx.Requester.ID == ctx.Guild.OwnerID ||
		ctx.Requester.ID == ctx.LibBot.Options.CreatorId ||
		ctx.Requester.ID == maID
}
