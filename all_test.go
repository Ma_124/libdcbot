package libbot

import (
	"testing"
	"github.com/bwmarrin/discordgo"
)

func Test(t *testing.T)  {
	InitFromEnv(&InitOption{
		CreatorId: maID,
	}, func(l *LibBot) {
		l.Commands["test-form"] = &CommandHandler{
			Handler: func(s string, args []string, ctx *Context) {
				a := &struct {
					A string `libbot:"A Alt text"`
					B string
				}{}

				ctx.RequestForm(a)
			},
		}

		l.Commands["test"] = &CommandHandler{
			Handler: func(s string, args []string, ctx *Context) {
				ctx.SendUI(&discordgo.MessageEmbed{
					Title: "Hello World",
				}, func(ctx *Context, ev *discordgo.MessageReaction) {
					msg, _ := ctx.LibBot.Session.ChannelMessage(ev.ChannelID, ev.MessageID)
					msg.Embeds[0].Description += "+ " + ev.Emoji.Name + "  \n\n"

					ctx.LibBot.Session.ChannelMessageEditComplex(&discordgo.MessageEdit{
						ID: ev.MessageID,
						Channel: msg.ChannelID,
						Embed: msg.Embeds[0],
					})
				}, func(ctx *Context, ev *discordgo.MessageReaction) {
					msg, _ := ctx.LibBot.Session.ChannelMessage(ev.ChannelID, ev.MessageID)
					msg.Embeds[0].Description += "- " + ev.Emoji.Name + "  \n\n"

					ctx.LibBot.Session.ChannelMessageEditComplex(&discordgo.MessageEdit{
						ID: ev.MessageID,
						Channel: msg.ChannelID,
						Embed: msg.Embeds[0],
					})
				})
			},
		}
	})
}
