package libbot

import (
	"reflect"
	"github.com/bwmarrin/discordgo"
	"time"
)

var formChans = make(map[string]chan string)

func (ctx *Context) RequestForm(f interface{}) (interface{}, error) {
	t := reflect.TypeOf(f).Elem()
	v := reflect.ValueOf(f).Elem()

	chann := make(chan string)
	formChans[ctx.Requester.ID] = chann

	dmctx, err := ctx.LibBot.NewContextFromDMChannelAndGuild(ctx.Requester.ID, ctx.Guild)

	loop: for i := 0; i < t.NumField(); i++ {
		fname, _ := t.Field(i).Tag.Lookup("libbot")
		if fname == "-" {
			continue
		}
		if fname == "" {
			fname = t.Field(i).Name
		}

		if err != nil {
			return nil, err
		}

		if v.Field(i).CanSet() {
			dmctx.SendMessage(fname + ":")
			select {
			case s := <- chann:
				v.Field(i).SetString(s)
			break
			case <- time.After(10 * time.Minute):
				dmctx.SendMessage("Sorry, your session timed out.")
				break loop
				break
			}
		} else {
			println("Cannot set " + t.Field(i).Name)
		}
	}

	formChans[ctx.Requester.ID] = nil

	return f, nil
}

func dmMessageCreateHandler(m *discordgo.MessageCreate, ctx *Context) {
	if formChans[ctx.Requester.ID] != nil {
		formChans[ctx.Requester.ID] <- m.Content
	}
}
