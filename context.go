package libbot

import (
	"github.com/bwmarrin/discordgo"
)

type Context struct {
	// A pointer to the `LibBot` instance
	LibBot *LibBot

	// The channel ID of the request. Send responses here.
	ChannelId string

	// The channel of the request. Send responses here.
	Channel *discordgo.Channel

	// The requester.
	Requester *discordgo.User

	// The guild of the request.
	Guild *discordgo.Guild

	// Config for the guild.
	Config *Config
}

// Creates a context from a message.
func (l *LibBot) NewContextFromMessage(m *discordgo.Message) (ctx *Context, err error) {
	ctx = &Context{
		LibBot: l,

		ChannelId: m.ChannelID,
		Requester: m.Author,
	}

	ch, err := l.Session.Channel(m.ChannelID)
	if err == nil {
		ctx.Channel = ch
	}

	g, err := l.Session.Guild(ch.GuildID)
	if err == nil {
		ctx.Guild = g
	}

	ctx.populateContextConfig(ch.GuildID)

	return
}

// Creates a context from a DM channel.
func (l *LibBot) NewContextFromDMChannel(reqId string) (ctx *Context, err error) {
	ctx = &Context{
		LibBot: l,
	}

	ch, err := ctx.LibBot.Session.UserChannelCreate(reqId)
	ctx.ChannelId = ch.ID
	ctx.Channel = ch

	ctx.Requester = ch.Recipients[0]
	ctx.Guild = nil
	ctx.Config = nil

	return
}

// Creates a context from a DM channel and populates `Guild` and `Config`.
func (l *LibBot) NewContextFromDMChannelAndGuild(reqId string, guild *discordgo.Guild) (ctx *Context, err error) {
	ctx, err = l.NewContextFromDMChannel(reqId)

	ctx.Guild = guild
	ctx.populateContextConfig(guild.ID)

	return
}

func (l *LibBot) NewContextFromMessageReaction(msgReact *discordgo.MessageReaction) (ctx *Context, err error) {
	ctx = &Context{
		LibBot: l,

		ChannelId: msgReact.ChannelID,
	}

	ctx.Channel, err = l.Session.Channel(ctx.ChannelId)
	ctx.Requester, err = l.Session.User(msgReact.UserID)
	ctx.Guild, err = l.Session.Guild(ctx.Channel.GuildID)
	ctx.populateContextConfig(ctx.Guild.ID)

	return
}

func (ctx *Context) populateContextConfig(guildId string)  {
	ctx.Config = ctx.LibBot.Config[guildId]
	if ctx.Config == nil {
		ctx.Config = &Config{
			CommandPrefix: "!",
			Permissions: make(map[string]PermissionConfig),
		}
	}
}

// Sends a message on `ctx.ChannelId`
func (ctx *Context) SendMessage(s string) {
	ctx.LibBot.Session.ChannelMessageSend(ctx.ChannelId, s)
}
